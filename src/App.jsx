import React from "react";
import { Provider } from "react-redux";
import { SnackbarProvider } from "notistack";
import Router from "./Router";

import { store } from "./store";

import "./styles/main.scss";

function App() {
  return (
    <Provider store={store}>
      <SnackbarProvider>
      <Router />
      </SnackbarProvider>
    </Provider>
  );
}

export default App;

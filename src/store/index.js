import { configureStore } from "@reduxjs/toolkit";

import userReducer from "./userSlice"
import postReducer from "./postSlice"
import usersReducer from "./usersSlice"

export const store = configureStore({
  reducer: {
    user: userReducer,
    post: postReducer,
    users: usersReducer
  },
});

import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const endpoint = process.env.REACT_APP_ENDPOINT || "";

export const authUser = createAsyncThunk(
  "user/authUser",
  async function ({ email, password }, { rejectWithValue }) {
    try {
      const response = await fetch(
        `${endpoint}/users?email=${email}&password=${password}`
      );

      if (!response.ok) {
        throw new Error("Server error!");
      }

      const data = await response.json();

      if (data.length < 1) {
        throw new Error("There is no such user!");
      }

      localStorage.setItem("userEmail", email);
      localStorage.setItem("userPassword", password);

      return data[0];
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const registUser = createAsyncThunk(
  "user/registUser",
  async ({ email, password }) => {
      return fetch(`${endpoint}/users`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            email,
            password,
          }),
      }).catch('Failed to register user!')
          .then((res) => res.json())
          .then((res) => res)
  },
)

export const updateUserData = createAsyncThunk(
  'user/updateUserData',
  async function (
    { avatar, name, description, email },
    { rejectWithValue, getState }
  ) {
    const user = getState().user.user

    try {
      const userBody = {}

      if (avatar) {
        userBody.avatar = avatar
      }
      if (name) {
        userBody.name = name
      }
      if (description) {
        userBody.description = description
      }
      if (email) {
        userBody.email = email
      }

      const userDataResponse = await fetch(`${endpoint}/users/${user.id}`, {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(userBody),
      })

      if (!userDataResponse.ok) {
        throw new Error('Server error!')
      }

      return userDataResponse.json()
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)

const userSlice = createSlice({
  name: "user",
  initialState: { user: null, isLoading: false, isError: false, error: "" },
  reducers: {
    // Всегда синхронные
    // state иммутабельный
    logOut(state) {
      state.user = null;
      localStorage.setItem("userEmail", "");
      localStorage.setItem("userPassword", "");
    },
  },
  extraReducers: {
    [authUser.pending]: (state) => {
      state.isLoading = true;
    },
    [authUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [authUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.user = action.payload;
    },
    [registUser.pending]: (state) => {
      state.isLoading = true;
    },
    [registUser.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [registUser.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.user = action.payload;
    },
    [updateUserData.fulfilled]: (state, action) => {
      state.isLoading = false
      state.isError = false
      state.error = ''
      state.user = action.payload
      localStorage.setItem('userEmail', action.payload.email)
    },
  },
});

export const { signIn, logOut } = userSlice.actions;

export default userSlice.reducer;

import React, { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";

import { getPosts } from "../../store/postSlice";

import { Link } from "react-router-dom";

import s from "./OnePost.module.scss";

function OnePost() {
  const { user } = useSelector((state) => state.user)
  const [page, setPage] = useState(1);
  const { post } = useSelector((state) => state.post);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPosts({ limit: 9, page: page, isExpanded: true }));
  }, []);
  
  return (
    <div className={s.posts}>
      {post.map((elem) => (
        <>
          <div key={elem.id}>
            <Link to={`/postfile/${elem.id}`}>
              <h3 className={s.usertext}>{user.name}</h3>
              <h3 className={s.usertitle}>{elem.title}</h3>
              <img className={s.fill} src={elem.userimage} alt="" />
            </Link>
          </div>
        </>
      ))}
    </div>    
  );
}

export default OnePost;
import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { authUser } from "./store/userSlice";
import AuthorizationPage from "./pages/AuthorizationPage";
import FeedPage from "./pages/FeedPage";
import PostCreatePage from "./pages/PostCreatePage";
import LoadingPage from "./pages/LoadingPage";
import Layout from "./layout/Layout";
import FeedPageSettings from "./pages/FeedPageSettings";
import PostPage from "./pages/PostPage";
import RegisterPage from "./pages/RegisterPage";
import SettingPage from "./pages/SettingPage";
import UsersPage from "./pages/UsersPage";

function Router() {
  // const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const { user, isLoading } = useSelector((state) => state.user);

  const email = localStorage.getItem("userEmail") || "";
  const password = localStorage.getItem("userPassword") || "";

  useEffect(() => {
    if (email && password) {
      dispatch(authUser({ email, password }));
    }
  }, [dispatch, email, password]);

  const isLoadingPage = !user && isLoading && email && password;
  const isAuthorizationPage = !user && !isLoading && !email && !password;
  return (
    <BrowserRouter>
      <Routes>
      {isLoadingPage && 
          <Route path="*" element={<LoadingPage />} />
        }
        {user && (
        <>
        <Route path="/" element={<Layout />}>
              <Route index element={<FeedPage />} />
              <Route path="profile" element={<FeedPage />} />
              <Route path="createPost" element={<PostCreatePage />} />
              <Route path="postfile" element={<PostPage />} />
              <Route path="postfile/:id" element={<PostPage />} />
              <Route path="feedsettings" element={<FeedPageSettings />} />
              <Route path="settings" element={<SettingPage />} />
              <Route path="users" element={<UsersPage />} />
            </Route>        
        </>
        )}
        {isAuthorizationPage && (
          <>
          <Route path="/registration" element={<RegisterPage />} />
          <Route path="*" element={<AuthorizationPage />} />
          </>
        )}
        {/* <Route path="*" element={<RegisterPage />} /> */}
      </Routes>
    </BrowserRouter>
  );
}

export default Router;

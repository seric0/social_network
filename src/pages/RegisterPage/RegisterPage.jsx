import React, { useState } from "react";
import { useDispatch } from "react-redux";
// import { Link, Navigate } from "react-router-dom"

import { registUser } from "../../store/userSlice";
import Input from '../../components/Input'
import Button from '../../components/Button'
import { useNavigate } from "react-router-dom";

import { useSnackbar } from "notistack";

import AuthContainer from "../../components/AuthContainer";

import s from "./RegisterPage.module.scss";

function RegisterPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    dispatch(registUser({ email, password })).then((res) => {
      enqueueSnackbar({
        variant: "success",
        message: "Пользователь создан",
      }); 
      navigate("/");
    }
    )};

  return (
    <AuthContainer>
      <form className={s.form} onSubmit={onSubmit}>
        <div className={s.inputs}>
          <label className={s.label}>
            Email address
            <Input                            
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label className={s.label}>
            Password
            <Input                            
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <label className={s.label}>
            Confirm
            <Input                          
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <input type="checkbox" className={s.chekbox} />I accept the terms and privacy policy
        </div>
        <Button type="submit" className={s.button}>
          Register
        </Button>
      </form>
    </AuthContainer>
  );
}

export default RegisterPage;

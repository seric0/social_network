import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import TextField from "@mui/material/TextField";
// import Button from "@mui/material/Button";
import { useSnackbar } from "notistack";

import { savePost } from "../../store/postSlice";

import s from "./PostCreatePage.module.scss";

function PostCreatePage() {  
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useSelector((state) => state.user);
  const [userimage, setUserimage] = useState("");
  const [title, setTitle] = useState("");
  const [usertext, setUsertext] = useState("");
  
  const onUsertextChange = (e) => {
    setUsertext(e.target.value);
  };

  const onTitleChange = (e) => {
    setTitle(e.target.value);
  };

  const onUserimageChange = (e) => {
    setUserimage(e.target.value);
  };
    
  const onSubmitLocal = (e) => {
    e.preventDefault();

  let userId = user.id; 

    dispatch(savePost({ userimage, title, usertext, userId }));
    enqueueSnackbar({
      variant: "success",
      message: "Пост создан",
    }); 
    setTimeout(function () {
      window.location.href = "/profile";
   }, 4000)
  };

  return (
    <div className={s.feedPage}>
      <div className={s.container}>
        <div className={s.content}>
          <div className={s.headerText}>Post creation</div>
          <div className={s.headerPosts}>
            <form onSubmit={onSubmitLocal}>
              <div className={s.imageText}>Image url</div>
              <input
                type="text"
                className={s.imageEdit}
                required
                value={userimage}
                onChange={onUserimageChange}
              />
              <div className={s.titleText}>Title</div>
              <input
                type="text"
                className={s.titleEdit}
                required
                value={title}
                onChange={onTitleChange}
              />
              <div className={s.mainText}>Text</div>
              <textarea
                className={s.mainEdit}
                required
                value={usertext}
                onChange={onUsertextChange}
              />              
              <button type="submit" className={s.formButtonSave}>
                Create
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PostCreatePage;

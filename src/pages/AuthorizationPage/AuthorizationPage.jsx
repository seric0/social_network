import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { Link } from "react-router-dom"

import { authUser } from "../../store/userSlice";

import AuthContainer from "../../components/AuthContainer";
import Input from '../../components/Input'
import Button from '../../components/Button'

import { useSnackbar } from "notistack";

import s from "./AuthorizationPage.module.scss";

function AuthorizationPage() {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const { isLoading } = useSelector((state) => state.user);
  // const [isregistration, setRegistration] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    dispatch(authUser({ email, password }));
    enqueueSnackbar({
      variant: "success",
      message: "Пользователь авторизован",
    }); 
  };

  return (
    <AuthContainer>
      <form className={s.form} onSubmit={onSubmit}>
        <div className={s.inputs}>
          <label className={s.label}>
            Email address
            <Input
              type="text"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label className={s.label}>
            Password
            <Input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
        </div>
        <Button type="submit" disabled={isLoading}>
          {isLoading ? "Loading..." : "Sign in"}
        </Button>
      </form>
    </AuthContainer>
  );
}

export default AuthorizationPage;

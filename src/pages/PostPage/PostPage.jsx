import React, { useState, useEffect } from "react";
// import TextField from "@mui/material/TextField";
// import Button from "@mui/material/Button";

import { useParams } from "react-router-dom";

import { useSelector, useDispatch } from 'react-redux';

import { deletePost } from "../../store/postSlice";

import s from "./PostPage.module.scss";

function PostPage() {    
    const { user } = useSelector((state) => state.user)
    const { id } = useParams();
    const dispatch = useDispatch();
    const [email, setEmail] = useState("");    
    const onSubmit = (e) => {
        e.preventDefault();        
        dispatch(deletePost({ id }));
        setTimeout(function () {
            window.location.href = "/profile";
         }, 4000)
      };
    const [post, setPosts] = useState([]);
    const endpoint = 'http://localhost:3001'
    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch(`${endpoint}/posts/${id}`)
            const data = await res.json()            
            setPosts(data)
            return data;
        }
        fetchData()
    }, [])  
    if (post.id == null) {
        return (
            <div className={s.feedPage}>
            <div className={s.container}>
                <div className={s.content}>
                <p className={s.post_message}>Данного поста не существует.</p>
            </div>
            </div>
            </div>
        );
    }
    return (        
        <div className={s.feedPage}>
            <div className={s.container}>
                <div className={s.content}>
                <div className={s.headerPosts}>
                <div className={s.headerText}>{post.title}</div>
                <form onSubmit={onSubmit}>
                    <img className={s.image} src={post.userimage} alt="" />
                    <textarea
                        className={s.postEdit}
                        required
                        value={post.usertext}                        
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <div className={s.postUser}>
                        <img className={s.imageSmall} src={user.avatar} alt="" />
                        <p className={s.postUserName}>{user.name}</p>
                    </div>
                    <button type="submit" className={s.formButtonDelete}>
                        Delete
                    </button>
                    <button type="submit" className={s.formButtonEdit}>
                        Edit
                    </button>
                </form>
            </div>
                </div>
            </div>
        </div>
    );
}

export default PostPage;

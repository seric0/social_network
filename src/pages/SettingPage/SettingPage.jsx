import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { logOut } from "../../store/userSlice";

import { updateUserData } from '../../store/userSlice'

import Button from '../../components/Button'

import { useSnackbar } from "notistack";

import s from "./SettingPage.module.scss";

function SettingPage() {
  const dispatch = useDispatch();  
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useSelector((state) => state.user)
  const [email, setEmail] = useState(user.email || '');
  const [avatar, setAvatar] = useState(user.avatar || '')
  const [name, setName] = useState(user.name || '')
  const [description, setDescription] = useState(user.description || '')

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(updateUserData({ email, avatar, name, description }))
    enqueueSnackbar({
      variant: "success",
      message: "Данные обновлены",
    }); 
    dispatch(logOut())
  };

  return (
    <div className={s.authorizationPage}>
      <div className={s.container}>
        <form className={s.form} onSubmit={onSubmit}>
          <div className={s.inputs}>
            <label className={s.label}>
              Email address
              <input
                type="text"
                className={s.input}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </label>
            <Button type="submit" className={s.button1}>
           Save
          </Button>
            <label className={s.label}>
             Avatar
              <input
                type="text"
                className={s.input}
                value={avatar}
                onChange={(e) => setAvatar(e.target.value)}
              />
            </label>
            <Button type="submit" className={s.button}>
           Save
          </Button>
            <label className={s.label}>
             Name
              <input
                type="text"
                className={s.input}
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </label>
            <Button type="submit" className={s.button}>
           Save
          </Button> 
            <label className={s.label}>
             Description
              <textarea                
                className={s.input}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </label>           
          </div>
          <Button type="submit" className={s.button}>
           Save
          </Button>
          <Button type="submit" className={s.buttonLog}>
           Log out
          </Button>
        </form>
      </div>
    </div>
  );
}

export default SettingPage;

import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { getPosts } from "../../store/postSlice";

import Button from '../../components/Button'

import { Link } from 'react-router-dom'

import s from "./FeedPageSettings.module.scss";

function FeedPageSettings() {
  const [page, setPage] = useState(1);
  const { post } = useSelector((state) => state.post);
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPosts({ userId: 1, limit: 9, page: page, isExpanded: true }));
  }, []);
  const onPrev = () => {
    if (page === 1) return;
    setPage((prev) => prev - 1);

    dispatch(
      getPosts({ userId: 1, limit: 9, page: page - 1, isExpanded: true })
    );
  };
  const onNext = () => {
    setPage((prev) => prev + 1);

    dispatch(
      getPosts({ userId: 1, limit: 9, page: page + 1, isExpanded: true })
    );
  };

  return (
    <div className={s.feedPage}>
      <div className={s.container}>
        <div className={s.content}>
          <div><img className={s.imageSetting} src={user.avatar} alt="" /></div>
          <div className={s.headerText}>{user.name}</div>
          <div className={s.headerEmail}>{user.email}</div>
          <div className={s.headerSlogan}>Yhea, helo there!</div>
          <div className={s.headerButtonFollow}>Follow</div>        
          <Link to="/settings" className={s.link}>
            <Button type="submit" className={s.button}>
              Settings
            </Button>
          </Link>
          <div className={s.headerPosts}>
            <div className={s.posts}>
              {post.map((elem) => (
                <>
                  <div key={elem.id}>
                  <Link to={`/postfile/${elem.id}`}>
                  <h3 className={s.usertext}>{user.name}</h3>
                  <h3 className={s.usertitle}>{elem.title}</h3>
                  <img className={s.fill} src={elem.userimage} alt="" />
                  </Link>
                  </div>
                </>
              ))}
            </div>
          </div>
          <button type="button" className={s.footerButtonPrev} onClick={onPrev}>
            Prev
          </button>
          <button type="button" className={s.footerButtonNext} onClick={onNext}>
            Next
          </button>
        </div>
      </div>
    </div>
  );
}

export default FeedPageSettings;

import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";

import { getPosts } from "../../store/postSlice";

import OnePost from "../../components/OnePost";

import s from "./FeedPage.module.scss";

function FeedPage() {  
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const onPrev = () => {
    if (page === 1) return;
    setPage((prev) => prev - 1);

    dispatch(
      getPosts({ limit: 9, page: page - 1, isExpanded: true })
    );
  };
  const onNext = () => {
    setPage((prev) => prev + 1);

    dispatch(
      getPosts({ limit: 9, page: page + 1, isExpanded: true })
    );
  };
  return (
    <div className={s.feedPage}>
      <div className={s.container}>
        <div className={s.content}>
        <div className={s.headerText}>Recent posts</div>
        <div className={s.headerButton}>
          <Link to="/createPost">Add new post</Link>
          </div>        
        <div className={s.headerPosts}>       
           <OnePost />
          </div>                         
          <button type="button" className={s.footerButtonPrev} onClick={onPrev}>
            Prev
          </button>
          <button type="button" className={s.footerButtonNext} onClick={onNext}>
            Next
          </button>
        </div>
      </div>
    </div>
  );
}

export default FeedPage;

import React, { useState, useEffect } from "react";

import { getUsers } from "../../store/usersSlice";

import { useDispatch, useSelector } from "react-redux";

import s from "./UsersPage.module.scss";

function UsersPage() {
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const { users } = useSelector((state) => state.users);
  useEffect(() => {
    dispatch(getUsers({ limit: 5, page: page, isExpanded: false }));
  }, []);
  const onPrev = () => {
    if (page === 1) return;
    setPage((prev) => prev - 1);

    dispatch(
      getUsers({ limit: 5, page: page - 1, isExpanded: false })
    );
  };
  const onNext = () => {
    setPage((prev) => prev + 1);

    dispatch(
      getUsers({ limit: 5, page: page + 1, isExpanded: false })
    );
  };
  return (
    <div className={s.feedPage}>
      <div className={s.container}>
        <div className={s.content}>
          <div className={s.headerText}>Users</div>
          <div className={s.headerUsers}>
            <div className={s.postUsers}>
              {users.map((elem) => (
                <>
                  <div className={s.user} key={elem.id}>
                    <h3 className={s.username}>{elem.name}</h3>
                    <h3 className={s.useremail}>{elem.email}</h3>
                    <btton className={s.followButton}>
                    Follow
                    </btton>
                    <img className={s.fill} src={elem.avatar} alt="" />
                  </div>                
          </>
        ))}
          </div>
        </div>
        <button type="button" className={s.footerButtonPrev} onClick={onPrev}>
          Prev
        </button>
        <button type="button" className={s.footerButtonNext} onClick={onNext}>
          Next
        </button>
      </div>
    </div>
    </div >
  );
}

export default UsersPage;
